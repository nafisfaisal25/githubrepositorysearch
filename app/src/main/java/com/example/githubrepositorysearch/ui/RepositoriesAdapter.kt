package com.example.githubrepositorysearch.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.githubrepositorysearch.R
import com.example.githubrepositorysearch.databinding.RepoItemViewBinding
import com.example.githubrepositorysearch.model.Repository

class RepositoriesAdapter(
    private val context: Context?,
    private val onItemClickListener: (Repository) -> Unit
): ListAdapter<Repository, RepositoriesAdapter.ViewHolder>(DiffCallback) {



    inner class ViewHolder(private var binding: RepoItemViewBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(repository: Repository) {
            if (repository == null) return
            binding.repoName.text = repository.fullName
            binding.repoUpdated.text = context?.getString(R.string.updated_time, repository.lastUpdated)
            binding.repoDescription.text = repository.description
            binding.repoStars.text = repository.stars.toString()
            itemView.setOnClickListener {
                onItemClickListener(repository)
            }
        }
    }



    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Repository>() {
            override fun areItemsTheSame(oldItem: Repository, newItem: Repository): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Repository, newItem: Repository): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RepoItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}