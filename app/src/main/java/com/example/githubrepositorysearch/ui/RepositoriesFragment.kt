package com.example.githubrepositorysearch.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.githubrepositorysearch.Injection
import com.example.githubrepositorysearch.R
import com.example.githubrepositorysearch.databinding.FragmentRepositoriesBinding
import com.example.githubrepositorysearch.viewmodels.RadioButton
import com.example.githubrepositorysearch.viewmodels.RepositoriesFragmentViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class RepositoriesFragment : Fragment() {

    private var _binding: FragmentRepositoriesBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: RepositoriesAdapter

    private val viewModel: RepositoriesFragmentViewModel by viewModels {
        Injection.provideGithubRepoViewModelFactory(requireContext())
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRepositoriesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView(view)
        handleRadioButton()
        subScribeObservers()
    }

    private fun subScribeObservers() {
        subscribeDataBase()
        subscribeRadioButtonChange()
        subscribeErrorHandlingObservers()
    }

    private fun subscribeErrorHandlingObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
            binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
        viewModel.errorMessage.observe(viewLifecycleOwner) { errorMessage ->
            if (errorMessage == null) {
                binding.errorText.visibility = View.GONE
                binding.retryButton.visibility = View.GONE
            } else {
                binding.errorText.visibility = View.VISIBLE
                binding.retryButton.visibility = View.VISIBLE
                Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_LONG).show()
            }
        }

        binding.retryButton.setOnClickListener {
            viewModel.fetchData()
        }
    }

    private fun subscribeRadioButtonChange() {
        viewModel.radioButtonUpdated.observe(viewLifecycleOwner) {
            if (it) {
                subscribeDataBase()
            }
        }
    }

    private fun subscribeDataBase() {
        lifecycleScope.launch {
            viewModel.retrieveDataFromLocalDB().collect {
                adapter.submitList(it)
            }
        }
    }

    private fun handleRadioButton() {
        //for configuration change
        if (viewModel.getSelectedRadioButton() == RadioButton.STAR) {
            binding.radioGroup.check(R.id.starButton)
        } else {
            binding.radioGroup.check(R.id.updatedDateButton)
        }

        binding.starButton.setOnClickListener {
            viewModel.setRadioButton(RadioButton.STAR)
        }

        binding.updatedDateButton.setOnClickListener {
            viewModel.setRadioButton(RadioButton.UPDATED_DATE)
        }
    }


    private fun setUpRecyclerView(view: View) {
        adapter = RepositoriesAdapter(context) {
            val action = RepositoriesFragmentDirections
                .actionRepositoriesFragmentToRepositoryDetailsFragment(
                    repository = it
                )
            view.findNavController().navigate(action)
        }
        binding.recyclerView.adapter = adapter
        val decoration = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        binding.recyclerView.addItemDecoration(decoration)
        detectScrollEnd()
    }

    private fun detectScrollEnd() {
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if (!recyclerView.canScrollVertically(1)
                    && newState == RecyclerView.SCROLL_STATE_IDLE
                ) {
                    //fetch data from web when local data base is out of data
                    viewModel.fetchData()
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}