package com.example.githubrepositorysearch.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.githubrepositorysearch.R
import com.example.githubrepositorysearch.databinding.FragmentRepositoryDetailsBinding
import com.example.githubrepositorysearch.model.Repository

class RepositoryDetailsFragment : Fragment() {

    private var _binding: FragmentRepositoryDetailsBinding? = null
    private val binding get() = _binding!!
    private lateinit var repository: Repository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            repository = RepositoryDetailsFragmentArgs.fromBundle(it).repository
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRepositoryDetailsBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.description.text = repository.description
        binding.ownerName.text = repository.repoOwnerName
        binding.repoUpdated.text = context?.getString(R.string.updated_time, repository.lastUpdated)

        Glide.with(requireContext())
            .load(repository.repoOwnerImageUrl)
            .error(R.drawable.ic_error)
            .placeholder(R.drawable.ic_person)
            .into(binding.ownerImage)
    }
}