package com.example.githubrepositorysearch.api

import com.example.githubrepositorysearch.data.QUERY
import com.example.githubrepositorysearch.data.QUERY_PAGE_SIZE
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface GithubService {
    /**
     * Get repos ordered by stars.
     */
    @GET("search/repositories")
    suspend fun searchRepos(
        @Query("q") query: String = QUERY,
        @Query("sort") sort: String,
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int = QUERY_PAGE_SIZE,
        @Query("order")order: String = "desc"
    ): RepoSearchResponse

    companion object {
        private const val BASE_URL = "https://api.github.com/"

        fun create(): GithubService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubService::class.java)
        }
    }
}