package com.example.githubrepositorysearch.api

import com.example.githubrepositorysearch.api.RepoOwner
import com.google.gson.annotations.SerializedName

data class RepositoryInfo(
    @SerializedName("id") val id: Long,
    @SerializedName("full_name") val fullName: String,
    @SerializedName("description") val description: String?,
    @SerializedName("stargazers_count") val stars: Int,
    @SerializedName("updated_at") val lastUpdated: String,
    @SerializedName("owner") val repoOwner: RepoOwner,
)
