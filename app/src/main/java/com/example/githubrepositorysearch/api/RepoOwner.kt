package com.example.githubrepositorysearch.api

import com.google.gson.annotations.SerializedName

data class RepoOwner(
    @SerializedName("login") val ownerName: String,
    @SerializedName("avatar_url") val imageUr: String,
)
