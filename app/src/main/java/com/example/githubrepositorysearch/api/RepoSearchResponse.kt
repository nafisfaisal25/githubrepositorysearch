package com.example.githubrepositorysearch.api

import com.google.gson.annotations.SerializedName


data class RepoSearchResponse(
    @SerializedName("items") val items: List<RepositoryInfo> = emptyList(),
)