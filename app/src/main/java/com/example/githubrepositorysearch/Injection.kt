package com.example.githubrepositorysearch

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.savedstate.SavedStateRegistryOwner
import com.example.githubrepositorysearch.api.GithubService
import com.example.githubrepositorysearch.data.GithubRepository
import com.example.githubrepositorysearch.database.RepositoryDatabase
import com.example.githubrepositorysearch.viewmodels.RepositoriesFragmentViewModel
import com.example.githubrepositorysearch.viewmodels.RepositoriesFragmentViewModelFactory

object Injection {

    /**
     * Creates an instance of [GithubRepository] based on the [GithubService] and a
     * [GithubLocalCache]
     */
    private fun provideGithubRepository(context: Context): GithubRepository {
        return GithubRepository(GithubService.create(), RepositoryDatabase.getDatabase(context))
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [ViewModel] objects.
     */
    fun provideGithubRepoViewModelFactory(context: Context): ViewModelProvider.Factory {
        return RepositoriesFragmentViewModelFactory(provideGithubRepository(context))
    }
}