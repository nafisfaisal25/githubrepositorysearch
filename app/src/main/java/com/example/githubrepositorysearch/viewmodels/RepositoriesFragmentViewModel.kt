package com.example.githubrepositorysearch.viewmodels

import androidx.lifecycle.*
import com.example.githubrepositorysearch.data.GithubRepository
import com.example.githubrepositorysearch.model.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okio.IOException
import retrofit2.HttpException

class RepositoriesFragmentViewModel(private val githubRepository: GithubRepository): ViewModel() {

    private var radioButtonState = RadioButton.STAR


    private var _radioButtonUpdated = MutableLiveData(false)


    val radioButtonUpdated: MutableLiveData<Boolean>
        get() = _radioButtonUpdated

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _errorMessage = MutableLiveData<String?>(null)
    val errorMessage: LiveData<String?>
        get() = _errorMessage

    init {
        //for first time app installation data load
        fetchData()
    }

    fun fetchData() {
        viewModelScope.launch {
            _errorMessage.value = null
            _isLoading.value = true
            try {
                githubRepository.fetchData(getSortedCriteria())
            } catch (exception: IOException) {
                _errorMessage.value = exception.message
            } catch (exception:HttpException) {
                _errorMessage.value = exception.message
            } finally {
                _isLoading.value = false
            }
        }
    }

    suspend fun retrieveDataFromLocalDB(): Flow<List<Repository>> {
        return  githubRepository.retrieveDataFromLocalDB(getSortedCriteriaForDb())
    }

    fun getSelectedRadioButton(): RadioButton {
        return RadioButton.STAR
    }

    fun setRadioButton(state: RadioButton) {

        if (state != radioButtonState) {
            viewModelScope.launch {
                githubRepository.resetPageNumber()
                radioButtonState = state
                clearRepositories()
                radioButtonUpdated.value = true
                fetchData()
            }
        }
    }

    private suspend fun clearRepositories() {
        githubRepository.clearRepositories()
    }

    private fun getSortedCriteria(): String {
        return if (radioButtonState == RadioButton.STAR) {
            "stars"
        } else {
            "updated"
        }
    }

    private fun getSortedCriteriaForDb(): String {
        return if (radioButtonState == RadioButton.STAR) {
            "stars"
        } else {
            "lastUpdated"
        }
    }

}

class RepositoriesFragmentViewModelFactory(
    private val githubRepository: GithubRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RepositoriesFragmentViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RepositoriesFragmentViewModel(githubRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}

enum class RadioButton{
    STAR,
    UPDATED_DATE
}