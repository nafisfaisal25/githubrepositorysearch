package com.example.githubrepositorysearch.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "repository")
data class Repository(
    @PrimaryKey @field:SerializedName("id") val id: Long,
    @field:SerializedName("full_name") val fullName: String,
    @field:SerializedName("description") val description: String?,
    @field:SerializedName("stargazers_count") val stars: Int,
    @field:SerializedName("updated_at") val lastUpdated: String,
    @field:SerializedName("owner_name") val repoOwnerName: String,
    @field:SerializedName("owner_image_url") val repoOwnerImageUrl: String,
): Serializable