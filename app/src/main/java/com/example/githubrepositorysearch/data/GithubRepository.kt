package com.example.githubrepositorysearch.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.githubrepositorysearch.api.GithubService
import com.example.githubrepositorysearch.database.RepositoryDatabase
import com.example.githubrepositorysearch.model.Repository
import kotlinx.coroutines.flow.Flow
import okio.IOException
import retrofit2.HttpException

const val QUERY_PAGE_SIZE = 10
const val QUERY = "android in:name,description"
const val INITIAL_PAGE_NUMBER = 1

class GithubRepository(
    private val service: GithubService,
    private val database: RepositoryDatabase
) {
    private var currentPage = INITIAL_PAGE_NUMBER

    suspend fun fetchData(sortedCriteria: String) {

        try {
            val fetchedData = service.searchRepos(sort = sortedCriteria, page = currentPage).items.map {
                Repository(
                    it.id,
                    it.fullName,
                    it.description,
                    it.stars,
                    it.lastUpdated,
                    it.repoOwner.ownerName,
                    it.repoOwner.imageUr
                )
            }
            currentPage++
            insertDataIntoDataBase(fetchedData)
        } catch (exception: IOException) {
            throw(exception)
        } catch (exception: HttpException) {
            throw(exception)
        }
    }

    private suspend fun insertDataIntoDataBase(repoList: List<Repository>) {
        database.repositoryDao().insertAll(repoList)
    }

    suspend fun retrieveDataFromLocalDB(sortedCriteria: String): Flow<List<Repository>> {
        calculateCurrentPage()
        return if (sortedCriteria == "stars") {
            database.repositoryDao().getReposOrderByStar()
        } else {
            database.repositoryDao().getReposOrderUpdatedDate()
        }

    }

    suspend fun clearRepositories() {
        database.repositoryDao().clearRepositories()
    }

    private suspend fun calculateCurrentPage() {
        currentPage = database.repositoryDao().getNumberOfRepository() / QUERY_PAGE_SIZE
        currentPage++
    }

    fun resetPageNumber() {
        currentPage = INITIAL_PAGE_NUMBER
    }
}