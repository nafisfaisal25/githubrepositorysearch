package com.example.githubrepositorysearch.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.githubrepositorysearch.model.Repository

@Database(
    entities = [Repository::class],
    version = 2,
    exportSchema = false
)
abstract class RepositoryDatabase: RoomDatabase() {
    abstract fun repositoryDao(): RepositoryDao

    companion object {

        @Volatile
        private var INSTANCE: RepositoryDatabase? = null

        fun getDatabase(context: Context): RepositoryDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    RepositoryDatabase::class.java,
                    "Github_repo.db")
                    .build()
                INSTANCE = instance

                instance
            }
        }
    }
}
