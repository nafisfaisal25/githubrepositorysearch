package com.example.githubrepositorysearch.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.githubrepositorysearch.model.Repository
import kotlinx.coroutines.flow.Flow

@Dao
interface RepositoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(repos: List<Repository>)

    @Query(
        "SELECT * FROM repository WHERE " +
                "fullName LIKE :queryString OR description LIKE :queryString " +
                "ORDER BY stars DESC"
    )
    fun getRepos(queryString: String): LiveData<Repository>

    @Query("SELECT * FROM repository ORDER BY stars DESC")
    fun getReposOrderByStar(): Flow<List<Repository>>

    @Query("SELECT * FROM repository ORDER BY lastUpdated DESC")
    fun getReposOrderUpdatedDate(): Flow<List<Repository>>

    @Query("DELETE FROM repository")
    suspend fun clearRepositories()

    @Query("SELECT COUNT(*) FROM repository")
    suspend fun getNumberOfRepository(): Int
}